package net.ShakhraiRuslan.SpringBootThymeleafCrud.repository;

import net.ShakhraiRuslan.SpringBootThymeleafCrud.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,Long> {

}
